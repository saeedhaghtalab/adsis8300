#!/bin/bash


for i in 1 2 3 4 5 6 7 8 9 10; do
    echo "================================================================="
    n=$((i*16))
    echo "using number of samples : $n"
    c=$((1024*n))
    cb=$((c*2))
    b=$((n*2))
    d=$((n*4))
    db=$((d*2))
    echo "clearing memory address registers 0x120 .. 0x12A .."
    for r in 120 121 122 123 124 125 126 127 128 129 12a; do
        ../../bin/linux-x86_64/sis8300drv_reg /dev/sis8300-5 $r -w 0
    done
    echo "clearing memory $c samples, $cb bytes .."
    ../../bin/linux-x86_64/sis8300drv_mem /dev/sis8300-5 -w 0 -n $c
    echo "acquiring $n samples, $b bytes, from 1 channel .."
    ../../bin/linux-x86_64/sis8300drv_acq /dev/sis8300-5 -a 1 -n $n
    echo "dumping memory $s samples, $db bytes .."
    ../../bin/linux-x86_64/sis8300drv_mem /dev/sis8300-5 -n $d -p
    echo "dumping memory address registers 0x120 .. 0x12A .."
    for r in 120 121 122 123 124 125 126 127 128 129 12a; do
        ../../bin/linux-x86_64/sis8300drv_reg /dev/sis8300-5 $r -v
    done
done


